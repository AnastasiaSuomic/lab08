﻿using System.Collections.Generic;
using System.Linq;

public class HistoryManager
{
    public List<HistoryItem> Items;

    private static HistoryManager _instance;
    //реализация паттерна синглтон
    public static HistoryManager Instance{get{if (_instance == null) _instance = new HistoryManager();
        return _instance;
    }}

    private HistoryManager()
    {
        _instance = this;
        LoadItems();
    }

    public void LoadItems()
    {
        Items = SqliteController.Instance.GetHistoryItems().ToList();
        if (Items == null)
            Items = new List<HistoryItem>();
    }

    public void Clear()
    {
        Items?.Clear();
        SqliteController.Instance.ClearItems();
    }
    
    public void AddMessageToHistory(string message, string result)
    {
        var item = new HistoryItem(message, result);
        Items.Add(item);
        SqliteController.Instance.InsertItem(item);

    }
}