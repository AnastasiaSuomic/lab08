﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class AnimationViewer : MonoBehaviour
{
    [SerializeField] private Button buttonScale;
    [SerializeField] private Button buttonRotate;
    [SerializeField] private Button buttonMove;
    [SerializeField] private Button buttonAlpha;
    [SerializeField] private Button buttonClose;
    [SerializeField] private Image image;

    private void Awake()
    {
        buttonScale.onClick.AddListener(ButtonScaleOnClick);
        buttonRotate.onClick.AddListener(ButtonRotateOnClick);
        buttonMove.onClick.AddListener(ButtonMoveOnClick);
        buttonAlpha.onClick.AddListener(ButtonAlphaOnClick);
        buttonClose.onClick.AddListener(ButtonCloseOnClick);
    }

    private void ButtonCloseOnClick()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator AlphaCoroutine()
    {
        yield return AlphaCoroutine(1, 0, image);
        yield return AlphaCoroutine(0, 1, image);
    }
    
    private IEnumerator AlphaCoroutine(float start, float target, Image image)
    {
        var t = 0f;
        var speed = 0.5f;
        var alpha = 1f;
        while (t < 1)
        {
            alpha = Mathf.Lerp(start, target, t);
            var color = image.color;
            color.a = alpha;
            image.color = color;
            t += speed * Time.deltaTime;
            yield return null;
        }
        
        var newColor = image.color;
        newColor.a = target;
        image.color = newColor;
    }
    
    private void ButtonAlphaOnClick()
    {
        StartCoroutine(AlphaCoroutine());
    }

    private IEnumerator MoveCoroutine()
    {
        yield return MoveCoroutine(0, 450, image);
        yield return MoveCoroutine(450, -450, image);
        yield return MoveCoroutine(-450, 0, image);
    }
    
    private IEnumerator MoveCoroutine(float start, float target, Image image)
    {
        var t = 0f;
        var speed = 0.5f;
        var x = 1f;
        var y = image.rectTransform.anchoredPosition.y;
        while (t < 1)
        {
            x = Mathf.Lerp(start, target, t);
            image.rectTransform.anchoredPosition = new Vector3(x, y, 0);
            t += speed * Time.deltaTime;
            yield return null;
        }
        image.rectTransform.anchoredPosition = new Vector3(target, y, 0);
    }
    
    private void ButtonMoveOnClick()
    {
        StartCoroutine(MoveCoroutine());
    }

    private IEnumerator RotateCoroutine()
    {
        yield return RotateCoroutine(0, 360, image);
    }
    
    private IEnumerator RotateCoroutine(float start, float target, Image image)
    {
        var t = 0f;
        var speed = 0.5f;
        var z = 1f;
        while (t < 1)
        {
            z = Mathf.Lerp(start, target, t);
            image.transform.localRotation = Quaternion.Euler(0,0, z);
            t += speed * Time.deltaTime;
            yield return null;
        }
        image.transform.localRotation = Quaternion.Euler(0,0, target);
    }
    
    private void ButtonRotateOnClick()
    {
        StartCoroutine(RotateCoroutine());
    }

    private IEnumerator ScaleCoroutine()
    {
        yield return ScaleCoroutine(1, 0, image);
        yield return ScaleCoroutine(0, 1, image);
    }
    
    private IEnumerator ScaleCoroutine(float start, float target, Image image)
    {
        var t = 0f;
        var speed = 0.5f;
        var x = 1f;
        var y = 1f;
        while (t < 1)
        {
            x = Mathf.Lerp(start, target, t);
            y = Mathf.Lerp(start, target, t);
            image.transform.localScale = new Vector3(x, y, 1);
            t += speed * Time.deltaTime;
            yield return null;
        }
        image.transform.localScale = new Vector3(target, target, 1);
    }
    
    private void ButtonScaleOnClick()
    {
        StartCoroutine(ScaleCoroutine());
    }
}